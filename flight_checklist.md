**************************************************************************************
**                                   PRE-FLIGHT                                     **
**************************************************************************************

1. Plug in battery, test each servo and propeller direction with remote
2. Ensure GPS has a fix on location indicated by the flashing green light on the GPS module.
     (Can take up to 5 minutes after boot)
3. SSH into the onboard computer
4. Start a tmux session with `tmux new -s <name>` (i.e. `tmux new -s flighttest`)
5. Run `roslaunch start_ros_flight launcher.launch bag:=true`
    Note: `roslaunch start_ros_flight launcher.launch` will start without rosbag
6. Place the plane on a level surface balanced on the airframe for proper calibration
7. Open a new tmux-pane with `ctrl+b, %` and call the calibration service for the airspeed, imu, and baro
    `./calibrate_sensors` is a quick bash script that will calibrate each of them for you.
8. Check that each of the rosflight parameters are set appropriately.
9. Echo a few topics to ensure data is streaming and appears accurate (i.e. `rostopic echo /airspeed`)
10. Secure the plane's cockpit
11. Launch plane





**************************************************************************************
**                                   POST-FLIGHT                                    **
**************************************************************************************

1. SSH back into the onboard computer
2. Reconnect to existing tmux session with `tmux a -t <name>` (i.e. `tmux a -t flighttest`)
3. `ctrl+c` each of the running terminals
4. If data was bagged, send the file to the base-station computer with `scp <path_to_file> <user@address:~>` 
    (i.e. `scp ~/bagfiles/flight_12-01-2018.bag autonomylab@192.168.0.121:~`)
5. On the base-station computer, rename/move the bag file to the appropriate location
6. Run the data plotting script on the bag file with `roscd ros_plotter/script` and `./plotter ~/bagfiles/test.bag`
    Note: The plotter script will also take an additional parameter 'save' if you want each of the plots to be
    saved as .png
7. If the flight ended without properly killing the flight process, the bag will first need to be reindexed before
    the data can be read. This is done with `rosbag reindex test.bag`. The old file can be safely deleted.
