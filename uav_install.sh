#!/bin/bash

# Create swap space
echo -e "${GN}----- Creating Swapspace -----${NC}"
sudo fallocate -l 3G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile


# common installs and cleans
echo -e "${GN}----- Common Installs -----${NC}"
sudo apt install git vim tmux htop tree openssh-server -y
rm -rf ~/Templates ~/Videos ~/Public ~/Pictures


#### ROS ####
echo -e "${GN}----- Installing ROS -----${NC}"
sudo add-apt-repository universe
sudo apt update
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
sudo apt update
sudo apt install ros-kinetic-desktop-full -y --allow-unauthenticated
sudo rosdep init
rosdep update
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential -y
sudo apt-get install ros-kinetic-geodesy

cd ~
mkdir bagfiles
mkdir -p catkin_ws/src
cd catkin_ws
catkin_make
cd src
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc

# ublox
echo -e "${GN}----- Cloning Ublox GPS -----${NC}"
cd ~/catkin_ws/src
git clone https://github.com/KumarRobotics/ublox.git
cd ~/catkin_ws
catkin_make

# rosflight
echo -e "${GN}----- Cloning Rosflight -----${NC}"
cd ~/catkin_ws/src
git clone --recurse-submodules https://github.com/rosflight/rosflight.git
cd rosflight
rosdep install --ignore-src rosflight
cd ~/catkin_ws/src
rosdep install --ignore-src --from-path rosflight
cd ~/catkin_ws
catkin_make

# rosplane
echo -e "${GN}----- Cloning Rosplane -----${NC}"
cd ~/catkin_ws/src
git clone https://gitlab.com/Randall.Christensen/usu-suas.git rosplane
cd ~/catkin_ws
catkin_make

# plotter
echo -e "${GN}----- Cloning Plotter -----${NC}"
cd ~/catkin_ws/src
git clone https://gitlab.com/Randall.Christensen/plotter.git
cd ~/catkin_ws
catkin_make

# update rosplane
#cd ~/catkin_ws/src/plotter/rosplane_update
#cp CMakeLists.txt waypoints.txt ~/catkin_ws/src/rosplane/rosplane
#cp src/path_planner_file.cpp ~/catkin_ws/src/rosplane/rosplane/src
#cd ~/catkin_ws
#catkin_make


# start_ros_flight
echo -e "${GN}----- Cloning Start_ros_flight -----${NC}"
cd ~/catkin_ws/src
git clone https://gitlab.com/Randall.Christensen/start_ros_flight.git
cd start_ros_flight
mv parameters.yml calibrate_sensors ~
mv drone_simple.yaml ~/catkin_ws/src/ublox/ublox_gps/config
cd ~/catkin_ws
catkin_make

#### Misc  ####
rospack profile
source ~/.bashrc
echo -e "${GN}----- Upgrading -----${NC}"
sudo apt upgrade
echo -e "${GN}----- Cleaning -----${NC}"
sudo apt clean
source ~/.bashrc
cd ~
echo -e "${GN}----- Enabling SSH -----${NC}"
sudo systemctl enable ssh
sudo systemctl start ssh

# remove swapfile
echo -e "${GN}----- Removing Swapfile -----${NC}"
sudo swapoff /swapfile
sudo rm /swapfile

echo -e "${GN}----- Adding User to groups -----${NC}"
sudo usermod -a -G plugdev $USER
sudo usermod -a -G dialout $USER

if id -nG "$USER" | grep -qw "dialout"; then
    :
else
    echo -e "${GN}----- Rebooting -----${NC}"
    sudo reboot
fi