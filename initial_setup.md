**************************************************************************************
**                                 INITIAL-SETUP                                    **
**************************************************************************************

1. You will need to edit the rosflight_io node to look for the flight controller on the 
    correct port as the default is incorrect. This can be done by executing
    `rosed rosflight rosflight_io.cpp` and changing the target port from '/dev/ttyUSB0'  
    to '/dev/ttyACM0' (Line 81)
2. Go back to the workspace and run re-make your files (`cd ~/catkin_ws` and `catkin_make`)
    This could take 2-3 minutes to complete.
3. The next step is to modify the startup launch file. As a quick intro, launch files are 
    used to start many nodes at the same time with given parameters. Our launch file for 
    this class starts the flight controller node, and some of the higher level
    planning nodes. Since these nodes have not yet been written, the current launch file
    will fail. You will need to modify the launch file found in
    '~/catkin_ws/src/start_ros_flight/launch' and make a few changes so that it can 
    used. (I would recommend keeping a copy of the current launcher.launch)
    In that file, remove lines 15-19 that start nodes from the "rosplane" package.
4. Now we need to write the fixed-wing parameters to the flight controller. Because the 
    defaults are for a quadrotor, if done incorrectly, you can burn out your servos.
    At this point, UNPLUG each of the servos from your flight controller.
4. Run the modified launch file `roslaunch start_ros_flight <file_name.launch>`
5. In a new terminal, (or tmux pane) run the following commands:
    `rosservice call /param_load_from_file ~/parameters.yml`
    `rosservice call /param_write`
    Each of these commands should return successfully with the parameters updated and
    written to the flight controller.
6. Kill the launch file in the other terminal (`ctrl+c`) and restart the flight controller
     by unplugging and plugging it back in.
7. Now check to make sure the parameters have been updated. Once reconnected, start the
    launch file again and in the other terminal run
    `rosservice call /param_get MOTOR_PWM_UPDATE`
    This should return the value '50'. If it does not, restart at step 5.
8. Now run the calibration script with ROS still running (this should be done with the arrows
    on the flight controller facing forward and the board level.) 
    `~/calibrate_sensors` 
9. Kill the running launch file, unplug the flight controller and reconnect the servos to the
    flight controller.
10. Now you'll need to make a few changes to the transmitter (joystick.) In the settings,
    change the stick mode to 'MODE2', make sure the throttle (THR) is reversed, and the AUX-CH
    are setup with:
    CH5: GEAR
    CH6: THRO HOLD
    CH7: NULL
    CH8: NULL
    CH9: NULL

    Change the vehicle type select to "acro".
11. At this point if you remote is bound, you should have control over your control surfaces.
12. To test the throttle, you will need to run the launch file and arm the motor (with the GEAR
    switch on the transmitter).

A good resource for troubleshooting is the ROSflight docs at http://docs.rosflight.org/en/latest/  





**************************************************************************************
**                             BEFORE FIRST FLIGHT                                  **
**************************************************************************************

1. You will need to follow the instructions found here:
    http://docs.rosflight.org/en/latest/user-guide/getting-started/
    to calibrate your ESC. (disregard step 1.1 in the "Calibrate ESCs" section.  Without proper 
	calibration the ESC will burn out during your flight. You may need to read throught the 
	"Parameter Configuration" page on the ROSflight docs if you are unsure how to set the 
	corresponding parameters.
2. Check out a toolbox and make sure you can connect to your plane through the router included
    in the toolbox. Since IP addresses can be recycled, you should consider reserving an IP address
    on the router so you don't have trouble connecting to your plane in the future through SSH.
3. I highly recommend learning and using the basics of tmux before your flight. This
    allows you to reconnect to a terminal running your code, even after you've lost connection
    to your plane.
4. Read through 'flight_checklist.md' and perform the steps there before each flight.
